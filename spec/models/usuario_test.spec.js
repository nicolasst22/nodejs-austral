
var mongoose = require('mongoose');
var Bicicleta = require('../../models/Bicicleta');
const Reserva = require('../../models/Reserva');
const Usuario = require('../../models/Usuario');

describe("Testing Usuarios", () => {
    beforeEach((done) => {
        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/test';
            // mongoose.disconnect();
            mongoose.connect(mongoDB, { useNewUrlParser: true });
            mongoose.set('useCreateIndex', true);
            var db = mongoose.connection;
            mongoose.is
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', () => {
                console.log('conectados!');
                done();
            });
            // done();
        });
    });

    afterEach((done) => {
        Reserva.deleteMany({}, (err, suc) => {
            if (err) {
                console.log('error eliminando reservas');
            }
            console.log("reseras eliminadas");
            Usuario.deleteMany({}, (err, suc) => {
                if (err) {
                    console.log('error eliminando usuarios');
                }
                console.log("usuarios eliminados");
                Bicicleta.deleteMany({}, (err, suc) => {
                    if (err) {
                        console.log('error eliminando bicis');
                    }
                    console.log("bicis eliminadas");
                    done();
                });
            });
        });
    });

    describe('Usuario reserva bici', () => {
        it('debe existir la reserva', (done) => {
            const usuario = new Usuario({ nombre: 'Nico' });
            usuario.save();
            var bici = new Bicicleta({ code: 1, color: 'verde', modelo: 'modelo' });
            bici.save();
            var hoy = new Date();
            var manana = new Date();
            manana.setDate(hoy.getDate() + 1);
            // done();
            usuario.reservar(bici.id, hoy, manana, (err, reserva) => {
                Reserva.find({}).populate('bicicleta').populate('usuario').exec((err, reservas) => {
                    if (err) {
                        console.log(err);
                    }
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(bici.code);
                    done();
                });
            });
        });
    });



})