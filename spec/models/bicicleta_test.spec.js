
var mongoose = require('mongoose');
var Bicicleta = require('../../models/Bicicleta');

describe("Testing Bicicletas", () => {
    beforeEach((done) => {
        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/test';
            // mongoose.disconnect();
            mongoose.connect(mongoDB, { useNewUrlParser: true });
            mongoose.set('useCreateIndex', true);
            var db = mongoose.connection;
            mongoose.is
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', () => {
                console.log('conectados!');
                done();
            });
            // done();
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) {
                console.log(err);
            } else {
                console.log('limpie la base');
                done();
            }

        });
    });


    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toEqual(0);
                done();
            });
        })
    });

    describe('create', () => {
        it('creo instancia', (done) => {
            var bici = Bicicleta.createInstance(1, 'verde', 'urbana', [-34.5, -54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            done();
        });
    });

    describe('add', () => {
        it('agrego bici', (done) => {
            var a = Bicicleta.createInstance(1, 'verde', 'urbana', [-34.5, -54.1]);
            Bicicleta.add(a, (err, bici) => {
                if (err) {
                    console.log(err);
                }
                Bicicleta.allBicis((err, bicis) => {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(a.code);
                    done();
                });
            });
        });
    });

    describe('findByCode', () => {
        it('busco bici', (done) => {
            var a = Bicicleta.createInstance(1, 'verde', 'urbana', [-34.5, -54.1]);
            Bicicleta.add(a, (err, bici) => {
                if (err) {
                    console.log(err);
                }
                var a2 = Bicicleta.createInstance(2, 'roja', 'urbana', [-34.5, -54.1]);
                Bicicleta.add(a2, (err, bici) => {
                    if (err) {
                        console.log(err);
                    }
                    Bicicleta.findByCode(1, (err, result) => {
                        expect(result.code).toEqual(a.code);
                        expect(result.color).toEqual(a.color);
                        done();
                    });
                });
            });
        });
    });
});
