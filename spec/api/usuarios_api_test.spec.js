// var Bicicleta = require('../../models/Bicicleta');
var request = require("request");
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Bicicleta = require('../../models/Bicicleta');
const Reserva = require('../../models/Reserva');
const Usuario = require('../../models/Usuario');
var server = require('../../bin/www');

describe("Testing Usuarios API", () => {
    beforeEach((done) => {
        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/test';
            // mongoose.disconnect();
            mongoose.connect(mongoDB, { useNewUrlParser: true });
            mongoose.set('useCreateIndex', true);
            var db = mongoose.connection;
            mongoose.is
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', () => {
                console.log('conectados!');
                done();
            });
            // done();
        });

    });
    afterEach((done) => {
        Reserva.deleteMany({}, (err, suc) => {
            if (err) {
                console.log('error eliminando reservas');
            }
            Usuario.deleteMany({}, (err, suc) => {
                if (err) {
                    console.log('error eliminando usuarios');
                }
                done();
            });
        });
    });



    describe('add', () => {
        it('post de create', (done) => {
            var header = { 'content-type': 'application/json' };
            var a = '{"nombre": "otro"}';
            request.post({
                headers: header, url: "http://localhost:3000/api/usuarios/create",
                body: a
            }, (error, res, body) => {
                expect(res.statusCode).toBe(201);
                Usuario.allUsers((err, users) => {
                    expect(users.length).toEqual(1);
                    expect(users[0].nombre).toEqual("otro");
                    done();
                });

            });
        })
    });

});








