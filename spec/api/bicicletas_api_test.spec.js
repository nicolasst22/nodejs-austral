// var Bicicleta = require('../../models/Bicicleta');
var request = require("request");
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Bicicleta = require('../../models/Bicicleta');
const Reserva = require('../../models/Reserva');
const Usuario = require('../../models/Usuario');
var server = require('../../bin/www');

describe("Testing Bicicletas API", () => {
    beforeEach((done) => {
        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/test';
            // mongoose.disconnect();
            mongoose.connect(mongoDB, { useNewUrlParser: true });
            mongoose.set('useCreateIndex', true);
            var db = mongoose.connection;
            mongoose.is
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', () => {
                console.log('conectados!');
                done();
            });
            // done();
        });

    });
    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, suc) => {
            if (err) {
                console.log('error eliminando bicis');
            }
            done();
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis((err, bicis) => {
                if (err) {
                    console.log(err);
                }
                expect(bicis.length).toEqual(0);
                done();
            });
        })
    });

    describe('bicicletas', () => {
        it("Status 200 obtener bicicletas", (done) => {
            Bicicleta.allBicis((err, bicis) => {
                if (err) {
                    console.log('error eliminando bicis');
                }
                expect(bicis.length).toBe(0);
                request.get("http://localhost:3000/api/bicicletas", (error, res, body) => {
                    expect(res.statusCode).toBe(200);
                    done();
                });
            });


        });
    });

    describe('add', () => {
        it('post de create', (done) => {
            var header = { 'content-type': 'application/json' };
            var a = '{"code":3,"color":"azul","modelo":"Tomaselli","ubicacion":[-27.36201,-55.881762]}';
            request.post({
                headers: header, url: "http://localhost:3000/api/bicicletas/create",
                body: a
            }, (error, res, body) => {
                expect(res.statusCode).toBe(201);
                var result = Bicicleta.findByCode(1, (err, result) => {
                    expect(result.color).toBe('azul');
                    expect(result.modelo).toBe('Tomaselli');
                    done();
                });

            });
        })
    });

});








