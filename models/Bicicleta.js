var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true }
    }
});

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}

bicicletaSchema.statics.add = function (bici, cb) {
    this.create(bici, cb);
}

bicicletaSchema.statics.findByCode = function (aCode, cb) {
    return this.findOne({ code: aCode }, cb);
}

bicicletaSchema.statics.removeByCode = function (aCode, cb) {
    return this.deleteOne({ code: aCode }, cb);
}



bicicletaSchema.methods.toString = function () {
    return 'code: ' + this.code + " | color: " + this.color + " | metodo: " + this.metodo + " | ubicacion: " + this.ubicacion;
}


bicicletaSchema.statics.allBicis = function (cb) {
    return this.find({}, cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);




// reemplazado por mongo

// var Bicicleta = function (id, color, modelo, ubicacion) {
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function () {
//     return 'id: ' + this.id + " color: " + this.color + " modelo: " + this.modelo;
// }

// Bicicleta.allBicis = [];
// Bicicleta.add = function(aBici){
//     Bicicleta.allBicis.push(aBici);
// }

// Bicicleta.remove = function(aBiciId){
//     abici = this.findById(aBiciId);
//     var index = Bicicleta.allBicis.indexOf(abici);
//     Bicicleta.allBicis.splice(index, 1);
// }

// Bicicleta.findById = function(aBiciId){
//     // console.log(aBiciId);
//     // console.log(Bicicleta.allBicis[0].id == aBiciId);
//     var bici = Bicicleta.allBicis.find(x => x.id == aBiciId);
//     if(bici){
//         // console.log(bici);
//         return bici;
//     }
//     else {
//         // console.log('nooooo');
//         throw new Error('No existe una bici con el id: '+aBiciId);
//     }

//     // if(bici)
//     //     return bici;
//     // else
//     //     throw new Error('No existe una bici con el id: '+aBiciId);
// }



// var a = new Bicicleta(1, 'rojo', "Topmega", [-27.361320, -55.890762]);
// var b = new Bicicleta(2, 'azul', "Trek", [-27.361020, -55.881762]);
// Bicicleta.add(a);
// Bicicleta.add(b);

// module.exports = Bicicleta;