var mongoose = require('mongoose');
var Reserva = require('./Reserva');
var Token = require('./Token');
var Schema = mongoose.Schema;
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const uniqueValidator = require("mongoose-unique-validator");
const mailer = require('../mailer/mailer');

const validateEmail = function(email){
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

const saltRounds = 10;

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, "El nombre del usuario es requerido"]
    },
    email: {
        type: String,
        trim: true,
        required: [true, "El email es requerido"],
        lowercase: true,
        unique: true,
        validate: [validateEmail, "Por favor ingrese un email valido"],
        match: [/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/]
    },
    password: {
        type: String,
        required: [true, "El password es requerido"]
    },
    passwordResetToken: String,
    passwordRestTokenExpires: Date,
    verificado: { 
        type: Boolean,
        default: false
    },
    googleId: {
        type: String,
        trim: true
    },
    facebookId: {
        type: String,
        trim: true
    },

});

usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save', function(next){
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(pass){
    return bcrypt.compareSync(pass, this.password);
}

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
    var reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId,
        desde: desde,
        hasta: hasta
    });
    reserva.save(cb);
}

usuarioSchema.methods.enviarEmailBienvenida = function (cb) {
     const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex') });
     const email_destino = this.email;
     token.save((err)=>{
         if (err){
             return console.log(err.message);
         }
         const mailOptions = {
            from: 'nicolasst22@gmail.com',
            to: email_destino,
            subject: 'Bienvenido a Red Bicicletas. Confirme su Cuenta',
            text: 'hola!\n\n Por favor verifica tu cuenta con el siguiente enlace '
            +'http://localhost:3000/token/confirmation/'+token.token
         };
         console.log('enviando mail');
         mailer.sendMail(mailOptions, (err)=>{
            if(err){
                return console.log(err.message);
            }
            console.log('Se envio email para verificacion de '+email_destino);
         });

     });
}

usuarioSchema.methods.resetPassword = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err) {
        if (err) { return cb(err); }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password de cuenta ',
            text: 'Hola,\n\n' + 'Por favor, para resetear el password de su cuenta haga click en este link: \n' 
                + 'http://localhost:3000/' + 'resetPassword\/' + token.token + '\n' };

        mailer.sendMail(mailOptions, function (err) {
            if (err) { return cb(err); }

            console.log('Se envio un email para resetear el password a: ' + email_destination + '.');

        });

        cb(null);

    });
}

usuarioSchema.statics.allUsers = function (cb) {
    return this.find({}, cb);
}

usuarioSchema.statics.findOrCreateByGoogle = function findOneOrCreate(condition, callback ) {
    const self = this;

    console.log('-----------condition');
    console.log(condition);
    self.findOne({
        $or: [ //or
            {'googleId': condition.id},
            {'email': condition.emails[0].value}
        ]
    }, (err, result) =>{
        console.log('--------result');
        console.log(result);
        if(result){
            callback(err, result);
        }else{
         //   console.log(condition);
            let values= {};
            values.googleId = condition.id,
            values.email = condition.emails[0].value,
            values.nombre = condition.displayName || 'Sin Nombre';
            values.verificado = true,
            values.password = condition._json.etag || 'desde google'; 
         //   console.log(values);
            self.create(values, (err, result)=>{
                if(err){
                    console.log("---------------------------");
            //        console.log(err);
                    return callback(err, result);
                }else{
                    console.log('====CREADO ===');
                    return callback(null, result);
                }
            });
        }
    });
};


usuarioSchema.statics.findOrCreateByFacebook = function findOneOrCreate(condition, callback ) {
    const self = this;

    console.log('-----------condition');
    console.log(condition);
    self.findOne({
        $or: [ //or
            {'facebookId': condition.id},
            {'email': condition.emails[0].value}
        ]
    }, (err, result) =>{
        console.log('--------result');
        console.log(result);
        if(result){
            callback(err, result);
        }else{
         //   console.log(condition);
            let values= {};
            values.facebookId = condition.id,
            values.email = condition.emails[0].value,
            values.nombre = condition.displayName || 'Sin Nombre';
            values.verificado = true,
            values.password = condition._json.etag || 'desde facebook'; 
         //   console.log(values);
            self.create(values, (err, result)=>{
                if(err){
                    console.log("---------------------------");
            //        console.log(err);
                    return callback(err, result);
                }else{
                    console.log('====CREADO ===');
                    return callback(null, result);
                }
            });
        }
    });
};

module.exports = mongoose.model('Usuario', usuarioSchema)
