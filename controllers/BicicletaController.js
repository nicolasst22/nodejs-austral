var Bicicleta = require('../models/Bicicleta');


exports.bicicletaList = function (req, res) {
    Bicicleta.allBicis(function (err, abicis) {
        res.render('bicicletas/index', { bicis: abicis });
    })
}

//get
exports.bicicletaCreate = function (req, res) {
    res.render('bicicletas/create');
}
//post
exports.bicicletaPersist = function (req, res) {
    Bicicleta
        .find({})
        .sort({ "code": -1 })
        .limit(1)
        .exec(function (err, doc) {
            let nextId = 1
            if (doc[0]) {
                let nextId = doc[0].code + 1
            }
            console.log(req.body);
            var bici = new Bicicleta({
                code: nextId, color: req.body.color, modelo: req.body.modelo,
                ubicacion: [req.body.lat, req.body.long]
            });
            bici.save();
            res.redirect('/bicicletas');
        });

}


//get
exports.bicicletaUpdate_get = function (req, res) {
    var biciId = req.params.biciId;
    Bicicleta.findByCode(biciId, (err, bici) => {
        res.render('bicicletas/update', { bici: bici });
    })

}

//post
exports.bicicletaUpdate_post = function (req, res) {
    var biciId = req.params.biciId;
    Bicicleta.findByCode(biciId, (err, bici) => {
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.lat, req.body.long];
        bici.save();
        res.redirect('/bicicletas');
    });

}


//get
exports.bicicletaRemove = function (req, res) {
    var biciId = req.params.biciId;
    Bicicleta.findByCode(biciId, (err, bici) => {
        bici.remove();
        res.redirect('/bicicletas');
    });
}
