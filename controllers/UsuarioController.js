var Usuario = require('../models/Usuario');

module.exports = {

    list: function (req, res, next) {
        Usuario.find({}, (err, usuarios) => {
            res.render('usuarios/index', { listUsuarios: usuarios });

        });
    },

    update_get: function (req, res, next) {
        Usuario.findById(req.params.id, function (err, usuario) {
            res.render('usuarios/update', { errors: {}, usuario: usuario });
        });
    },

    update_post: function (req, res, next) {

        var update_values = {
            nombre: req.body.nombre
        };

        Usuario.findByIdAndUpdate(req.params.id, update_values, (err, usuario) => {
            if (err) {
                console.log(err);
                res.render('usuarios/update', { errors: erro.errors, usuario: new Usuario({ nombre: req.body.nombre, email: req.body.email }) });
            }
            else {
                res.redirect('/usuarios');
                return;
            }
        });

    },

    create_get: function (req, res, next) {
        // const sgMail = require('@sendgrid/mail')
        // sgMail.setApiKey(process.env.SENDGRID_API_KEY)
        // const msg = {
        //     to: 'nicolasst22@gmail.com', // Change to your recipient
        //     from: 'nicolasst22@gmail.com', // Change to your verified sender
        //     subject: 'Sending with SendGrid is Fun',
        //     text: 'and easy to do anywhere, even with Node.js',
        //     html: '<strong>and easy to do anywhere, even with Node.js</strong>',
        // }
        // sgMail
        //     .send(msg)
        //     .then(() => {
        //         console.log('Email sent')
        //     })
        //     .catch((error) => {
        //         console.error(error)
        //     })
        res.render('usuarios/create', { errors: {}, usuario: new Usuario() });
    },

    create_post: function (req, res, next) {
        if (req.body.password != req.body.confirm_password) {
            console.log('no iguales');
            res.render('usuarios/create', { errors: { confirm_password: { message: 'No coincide con el password ingresado.' } }, usuario: new Usuario({ nombre: req.body.nombre, email: req.body.email }) });
            return;
        }
        Usuario.create({ nombre: req.body.nombre, email: req.body.email, password: req.body.password }, (err, newUsuario) => {
            if (err) {
                console.log('cagamo');
                console.log(err);
                res.render('usuarios/create', { errors: err.errors, usuario: new Usuario({ nombre: req.body.nombre, email: req.body.email }) });
            }
            else {
                console.log('bienvenida');
                newUsuario.enviarEmailBienvenida();
                res.redirect('/usuarios');
            }
        });
    },

    delete_post: function (req, res, next) {
        Usuario.findByIdAndDelete(req.body.id, function (err) {
            if (err)
                next(err);
            else
                res.redirect('/usuarios');
        });
    },

}