var Bicicleta = require('../../models/Bicicleta');


exports.bicicletaList = function (req, res, next) {
    Bicicleta.allBicis(function (err, bicis) {
        res.status(200).json({ Bicicletas: bicis });
    })
}

//get
exports.getBicicleta = function (req, res) {
    var biciId = req.params.biciId;
    var bici = Bicicleta.findById(biciId);
    res.status(200).json(bici);

}
//post
exports.create = function (req, res) {
    Bicicleta
        .find({})
        .sort({ "code": -1 })
        .limit(1)
        .exec(function (err, doc) {
            let nextId = 1
            if (doc[0]) {
                let nextId = doc[0].code + 1
            }
            var bici = new Bicicleta({
                code: nextId, color: req.body.color, modelo: req.body.modelo,
                ubicacion: [req.body.ubicacion[0], req.body.ubicacion[1]]
            });
            bici.save();
            res.status(201).json(bici);
        });
}


//post
exports.update = function (req, res) {
    var biciId = req.params.biciId;
    Bicicleta.findByCode(biciId, (err, bici) => {
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.lat, req.body.long];
        bici.save();
        res.status(200).json(bici);
    });

}


//post
exports.remove = function (req, res) {
    var biciId = req.params.biciId;
    Bicicleta.findByCode(biciId, (err, bici) => {
        bici.remove();
        res.status(204);
    });
}


