const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/Usuario');
const FacebookTokenStrategy = require('passport-facebook-token');

passport.use(new FacebookTokenStrategy({
    clientID: process.env.FACEBOOK_CLIENT_ID,
    clientSecret: process.env.FACEBOOK_CLIENT_SECRET},
    function(accessToken, refreshToken, profile, done){
        try{
            Usuario.findOrCreateByFacebook(profile, function(err, user){
                if(err){
                    console.log(err);
                    return done(err, user);
                }
                console.log("devuelvo despues de crear... todo ok");
                return done(err, user);
            });

        }catch(err2){
            console.log(err2);
            return done(err2, null);
        }
        // // // console.log("abajo del try");
        // // return done(null, profile);
    })
);



passport.use(new LocalStrategy((email, password, done) => {
        Usuario.findOne({email: email}, (err, usuario)=>{
            if(err){
                return done(err);
            }

            if(!usuario){
                return done(null, false, {message: 'Usuario o contraseña incorrectos'});
            }
            if(!usuario.validPassword(password)){
                return done(null, false, {message: 'Usuario o contraseña incorrectos'});
            }

            if(!usuario.verificado){
                return done(null, false, {message: 'Cuenta inactiva. Por favor verifiquela'});
            }

            return done(null, usuario);
        });
}

));


passport.serializeUser(function (user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
    Usuario.findById(id, (err, user) => {
        cb(err, user);
    });
});

module.exports = passport;