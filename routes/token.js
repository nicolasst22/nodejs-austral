var express = require('express');
var router = express.Router();
var tokenController = require('../controllers/TokenController');

/* GET users listing. */
router.get('/confirmation/:token', tokenController.confirmationGet);
module.exports = router;