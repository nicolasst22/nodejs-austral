var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  let loggedIn = false;
  let username = 'Invitado';
  if (req.user) {
    loggedIn = true;
    username = req.user.nombre;
  }
  res.render('index', { title: 'Express', loggedIn: loggedIn, username: username});
});

module.exports = router;
