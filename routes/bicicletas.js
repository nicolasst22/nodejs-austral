var express = require('express');

var router = express.Router();


var bicicletaController = require('../controllers/BicicletaController');

router.get('/', bicicletaController.bicicletaList);
router.get('/create', bicicletaController.bicicletaCreate);
router.get('/remove/:biciId', bicicletaController.bicicletaRemove);
router.get('/update/:biciId', bicicletaController.bicicletaUpdate_get);
router.post('/update/:biciId', bicicletaController.bicicletaUpdate_post);
router.post('/create', bicicletaController.bicicletaPersist);


module.exports = router;