var express = require('express');

var router = express.Router();


var usuarioController = require('../../controllers/api/UsuarioControllerAPI');

router.get('/', usuarioController.usuariosList);
router.post('/create', usuarioController.create);
router.post('/remove/:nombre', usuarioController.remove);
router.post('/reservar', usuarioController.reservar);
//router.get('/:nombre', usuarioController.getBicicleta);
// // router.post('/update/:biciId', usuarioController.bicicletaUpdate_post);
// router.post('/create', usuarioController.bicicletaPersist);


module.exports = router;