var express = require('express');

var router = express.Router();


var bicicletaController = require('../../controllers/api/BicicletaControllerApi');

router.get('/', bicicletaController.bicicletaList);
router.post('/create', bicicletaController.create);
router.post('/remove/:biciId', bicicletaController.remove);
router.post('/update/:biciId', bicicletaController.update);
router.get('/:biciId', bicicletaController.getBicicleta);
// // router.post('/update/:biciId', bicicletaController.bicicletaUpdate_post);
// router.post('/create', bicicletaController.bicicletaPersist);


module.exports = router;