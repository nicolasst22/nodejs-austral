//initialize the map on the "map" div with a given center and zoom
var map = L.map('main_map', {
    center: [-27.361087, -55.887071],
    zoom: 13
});


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([-27.361087, -55.887071]).addTo(map)
    .bindPopup('Punto de encuentro de ciclistas urbanos Posadas Misiones')
    .openPopup();


$.ajax({
    dataType: 'json',
    url: '/api/bicicletas',
    success: (data) => {
        data.bicicletas.forEach(element => {
            L.marker(element.ubicacion).addTo(map)
                .bindPopup('Bicicleta '+element.id);
        });
    }
})